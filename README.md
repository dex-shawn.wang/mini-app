# Visualization for Force Manipulation Skills

## Getting started

### Start on a server
SSH to a server
```
ssh dexterity@beast.dexterity.ai
```

Connect to tmux session
```
# tmux new -s force-manipulation-viz
tmux a -t force-manipulation-viz
```

Start the service:
```
cd ~/force-manipulation-viz/mini-app
docker-compose up
```

* Open browser at http://beast.dexterity.ai:1818

### Start on a local machine
* Start UI:
```
cd ui
npm install
make run
```

* Start backend with a new terminal
```
cd backend
poetry install
make run
```

* Open browser at http://localhost:1818

## Pulling logs from the robot server

Add trcomboserver to ssh config in `~/.ssh/config`, e.g. drex8:
```
Host drex8
  HostName trcombodrex8.tra.fdx.idm.dex.ai
  User dexterity
```

Add the ssh key to the robot server, with the loader password:
```
ssh-copy-id drex8
```

Pull logs from the robot server, in UI:
* In Overview, select **days** (e.g. 1 day), **loader** (e.g. drex8), then click button `Pull latest`
* Monitor backend terminal for downloading progress
* Click on the trace to visualize the trace (e.g. force history, end_effector_pose history, policy state, etc.)

## Example of a snapshot:
![Snapshot](assets/images/example_ui.png)