# Backend
```
poetry run flask --app src/app run
```

## Kill port if not available
`sudo lsof -i -P -n | grep 8811`
