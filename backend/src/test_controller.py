import matplotlib.pyplot as plt
import numpy as np


class SpringDynamics:
    def __init__(self, mass: float = 1.0):
        self.mass = mass
        self.x = 0.0
        self.x_dot = 0.0
        self.dt = 0.01

    def next(self, force: float):
        """
        F / m = a
        x = x + x_dot * dt
        x_dot = x_dot + force / m
        """
        self.x, self.x_dot = (
            self.x + self.x_dot * self.dt,
            self.x_dot - force / self.mass,
        )


class Controller:
    def __init__(self, kp: float, ki: float = 0.0, kd: float = 0.0):
        self.kp = kp
        self.ki = ki
        self.kd = kd

        self.previous_error = 0.0
        self.sum_error = 0.0

    def control(self, x: float, x_target: float):
        error = x_target - x
        error_dot = error - self.previous_error
        self.previous_error = error
        self.sum_error += error
        u = self.kp * error + self.kd * error_dot + self.ki * self.sum_error
        return u


def test_parameters(mass=0.01, kp=-0.0005, ki=0.0, kd=-0.05):
    dynamics = SpringDynamics(mass=mass)
    controller = Controller(kp=kp, ki=ki, kd=kd)

    dynamics.x = 20
    x_target = 0

    x_history = []
    for i in range(1000):
        force = controller.control(x=dynamics.x, x_target=x_target)
        dynamics.next(force)

        x_history.append(dynamics.x)
    return x_history


def plot_kd():
    for kd in np.linspace(-0.05, 0.05, 5):
        x_history = test_parameters(kd=kd)
        plt.plot(x_history, label=f"kd={kd}")
    plt.ylim([-100, 100])
    plt.legend()
    plt.show()


def plot_mass():
    for mass in np.linspace(0.001, 0.03, 5):
        x_history = test_parameters(mass=mass, kd=-0.05)
        plt.plot(x_history, label=f"mass={mass}")
    plt.ylim([-100, 100])
    plt.legend()
    plt.show()


def main():
    # plot_kd()
    plot_mass()


if __name__ == "__main__":
    main()
