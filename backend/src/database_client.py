import glob
import json
import os
from unittest.mock import Mock
from pymongo import MongoClient
import time

from data_parser import get_meta_data_from_filename

HOSTNAME = "0.0.0.0"
PORT = 27017
DATABASE_NAME = "force_manipulation_database"
COLLECTION_NAME = "tight_packing_collection"

GCP_FILENAME = "gcp_filename"


class DatabaseClient:
    def __init__(self, mock=False):
        if not mock:
            self._client = MongoClient(f"mongodb://{HOSTNAME}:{PORT}/")
        else:
            self._client = Mock()

        self._db = self._client[DATABASE_NAME]
        self._collection = self._db[COLLECTION_NAME]

    def insert_one(self, document):
        self._collection.insert_one(document)

    def delete_all(self):
        self._collection.delete_many({})

    def find(self, criteria, projection=None):
        return self._collection.find(criteria, projection)

    def upsert_document(self, criteria, new_document):
        self._collection.update_one(criteria, {"$set": new_document}, upsert=True)

    def rebuild_from_local_gcp(self):
        current_path = os.path.dirname(os.path.realpath(__file__))
        gcp_local_path = os.path.join(current_path, "..", "raw_logs")
        filenames = glob.glob(gcp_local_path + "/*/*/LOG_PACK_*.json")
        gcp_filenames = list(map(lambda x: "/".join(x.split("/")[-4:]), filenames))
        filenames_in_db = self._find_gcp_filename()
        new_gcp_filenames = list(set(gcp_filenames) - set(filenames_in_db))
        print("count of new gcp filenames: ", len(new_gcp_filenames))
        for i, gcp_filename in enumerate(new_gcp_filenames):
            print(
                f"processing {i + 1}/{len(new_gcp_filenames)}: {gcp_filename}\r", end=""
            )
            metadata = get_meta_data_from_filename(gcp_filename)
            metadata_with_gcp_filename = {GCP_FILENAME: gcp_filename, **metadata}
            self.upsert_document(
                {GCP_FILENAME: metadata_with_gcp_filename[GCP_FILENAME]},
                metadata_with_gcp_filename,
            )
        print(f"\nfinished processing {len(new_gcp_filenames)} new gcp filenames")

    def _find_gcp_filename(self):
        return list(
            map(
                lambda x: x[GCP_FILENAME],
                self.find(
                    {GCP_FILENAME: {"$exists": True}}, {"_id": 0, GCP_FILENAME: 1}
                ),
            )
        )


def main():
    database_client = DatabaseClient()
    # database_client.delete_all()
    database_client.rebuild_from_local_gcp()
    print(list(database_client.find({})))


if __name__ == "__main__":
    main()
