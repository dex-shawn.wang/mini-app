from matplotlib import pyplot as plt
import numpy as np
from dataloader import get_data
from proto_generator_state import Pose, ProtoGeneratorState


class ReplayManager:
    def __init__(self):
        pass

    def replay_velocity(self, data):
        tight_packing = data["msg"]["tightpack"]
        forces_history = tight_packing["force"]
        curr_pos_hisotry = np.array(tight_packing["curr_pos"])
        eef_pose_history = [Pose(position=pos) for pos in curr_pos_hisotry]
        timestamp_history = tight_packing["elapsed_time_list"]

        proto_generator_state = ProtoGeneratorState(maxlen=100)
        dz_history = []

        for i in range(len(forces_history)):
            proto_generator_state.add(
                forces_history[i], eef_pose_history[i], timestamp_history[i]
            )
            velocity = proto_generator_state.get_recent_velocity(duration=0.5)
            dz = velocity[2]
            dz_history.append(dz)
        return dz_history

    def visualize_dz_history(self, dz_history):
        plt.plot(dz_history)
        plt.show()

    def replay(self, loader_id, trace_id):
        data = get_data(loader_id, trace_id)
        dz_history = self.replay_velocity(data)
        self.visualize_dz_history(dz_history)


def main():
    loader_id = "drex7"
    trace_id = "72ac971ffc9c1659"

    replay_manager = ReplayManager()
    replay_manager.replay(loader_id, trace_id)


if __name__ == "__main__":
    main()
