import os
import time
import cv2
import numpy as np
import matplotlib.pyplot as plt
import json
import datetime
from scipy.spatial.transform import Rotation as R
import trimesh


def process_obstacles(data):
    obstacles = data["msg"]["obstacles"]

    pallet_dim_list = []
    pallet_position_list = []
    pallet_orientation_list = []
    for obstacle_name, obstacle_info in obstacles.items():
        if "state_service_box" in obstacle_name or "future_placed_box" in obstacle_name:
            # print(obstacle_name, obstacle_info)
            box_dim = obstacle_info["info"]["data"]
            pose_vector = obstacle_info["pose"]
            position = pose_vector[:3]
            quat_wxyz = pose_vector[3:]
            quat_xyzw = np.array(
                [quat_wxyz[1], quat_wxyz[2], quat_wxyz[3], quat_wxyz[0]]
            )
            orientation = R.from_quat(quat_xyzw)
            if "state_service_box" in obstacle_name:
                pallet_dim_list.append(box_dim)
                pallet_position_list.append(position)
                pallet_orientation_list.append(orientation)
            else:
                future_dim = box_dim
                future_position = position
                future_orientation = orientation

    tight_packing_data = data["msg"]["tightpack"]
    future_dim = tight_packing_data["box_dims_in_eef"]
    future_position = tight_packing_data["desired_box_pose"][:3]
    future_quat_wxyz = tight_packing_data["desired_box_pose"][3:]
    future_orientation = R.from_quat(future_quat_wxyz)

    future_box = [future_dim, future_position, future_orientation]
    print("future_box", future_box)
    pallet_state = [
        pallet_dim_list,
        pallet_position_list,
        pallet_orientation_list,
        future_box,
    ]
    return pallet_state


def create_mesh_box(box_dim, position, orientation) -> trimesh.Trimesh:
    rotation_matrix = orientation.as_matrix()
    tranform = np.identity(4)
    tranform[:3, :3] = rotation_matrix
    tranform[:3, 3] = position
    box: trimesh.Trimesh = trimesh.creation.box(extents=box_dim, transform=tranform)
    return box


def draw_lines(canvas, lines, origin, scale, color=(0, 0, 0)):
    for line in lines:
        pt1_x = (line[0][1] + origin[0]) * scale
        pt1_y = (line[0][2] + origin[1]) * scale
        pt2_x = (line[1][1] + origin[0]) * scale
        pt2_y = (line[1][2] + origin[1]) * scale
        cv2.line(
            canvas,
            (int(pt1_x), int(pt1_y)),
            (int(pt2_x), int(pt2_y)),
            color,
            1,
        )


def visualize_pallet(pallet_state, output_fn=None):
    scale = 200
    W, H = int(2.5 * scale), int(2.0 * scale)
    canvas = np.ones((H, W, 3), np.float32)
    (
        pallet_dim_list,
        pallet_position_list,
        pallet_orientation_list,
        future_box,
    ) = pallet_state
    future_dim, future_position, future_orientation = future_box

    plane_origin = np.array([1.3, 0, 0])
    plane_normal = np.array([1, 0, 0])

    canvas_origin = [0.95, 0.92]

    for i in range(len(pallet_dim_list)):
        box_dim = pallet_dim_list[i]
        position = pallet_position_list[i]
        orientation = pallet_orientation_list[i]

        box = create_mesh_box(box_dim, position, orientation)
        lines = trimesh.intersections.mesh_plane(box, plane_normal, plane_origin)
        if len(lines) == 0:
            print("no intersection", box_dim, position)

        color = (0, 0, 0)
        if np.sum(box_dim) == np.sum(future_dim):
            color = (0, 0, 1)

        draw_lines(canvas, lines, canvas_origin, scale=scale, color=color)

    box = create_mesh_box(future_dim, future_position, future_orientation)
    lines = trimesh.intersections.mesh_plane(box, plane_normal, plane_origin)
    draw_lines(canvas, lines, canvas_origin, scale=scale, color=(1, 0, 0))

    if output_fn is not None:
        cv2.imwrite(output_fn, (canvas[::-1, ::-1] * 255).astype(np.uint8))
