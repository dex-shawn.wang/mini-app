import io
import json
import os
import os.path
import sys
import threading

from flask import Flask, jsonify, make_response, send_file
from flask_cors import CORS

from analysis import process
from dataloader import DataLoader, get_tight_packing_data, load_trace_id_list

app = Flask(__name__)
CORS(app)

data_loader = DataLoader()

UPDATE_LOCK = threading.Lock()


@app.route("/")
def hello_world():
    return "<p>Hello, Tight Packing!</p>"


@app.route("/get-image/<loader_id>/<filename>", methods=["GET"])
def get_image(loader_id, filename):
    # retrieve the image data from a file or database
    image_data = open(f"figures/{loader_id}/{filename}", "rb").read()

    # set the response content type to 'image/png'
    headers = {"Content-Type": "image/png"}

    # return the image data as a response with appropriate headers
    return send_file(io.BytesIO(image_data), mimetype="image/png")


@app.route("/download-log/<loader_id>/<trace_id>", methods=["GET"])
def download_log(loader_id, trace_id):
    if not os.path.exists(f"data/{loader_id}/LOG_PACK_{trace_id}.json"):
        os.system(f"./scripts/get_planner_log.sh {trace_id} {loader_id}")
    try:
        process(loader_id, trace_id, always_update=True)
        data_loader.update(loader_id, [trace_id])
    except Exception as e:
        print(e)
    data = {"message": "Hello, World!", "number": 42}
    response = jsonify(data)
    return response, 200


@app.route("/get-data/<loader_id>/<trace_id>", methods=["GET"])
def get_data(loader_id, trace_id):
    if not os.path.exists(f"data/{loader_id}/LOG_PACK_{trace_id}.json"):
        os.system(f"./scripts/get_planner_log.sh {trace_id} {loader_id}")
    tight_packing_data = get_tight_packing_data(loader_id, trace_id)

    response = jsonify(tight_packing_data)
    return response, 200


@app.route("/update/<loader_id>/<recent_duation>", methods=["GET"])
def update(loader_id, recent_duation):
    print(f"UPDATE loader_id: {loader_id}, recent_duation: {recent_duation}")
    with UPDATE_LOCK:
        print("ACQUIRED UPDATE_LOCK")
        os.system(
            f"./scripts/get_planner_log.sh tight_packing {loader_id} {recent_duation}"
        )
        trace_id_list = load_trace_id_list(loader_id)
        data_loader.update(loader_id, trace_id_list, recent_duation)

    response, _ = get_trace_id()
    return response, 200


@app.route("/get-trace-id-list", methods=["GET"])
def get_trace_id():
    tight_packing_data = data_loader.data
    metadata = data_loader.metadata

    table = []
    for d in tight_packing_data:
        elem = d.split("/")
        loader_id = elem[0]
        trace_id = elem[1].split("_")[-1].rstrip(".json")

        # get metadata
        if d not in metadata:
            data_loader.update_meta(loader_id, [trace_id])

        done = None
        timestamp = None
        max_search_step = None

        if d in metadata:
            done = metadata[d]["done"]
            timestamp = metadata[d]["timestamp"]
            if "max_search_step" in metadata[d]:
                max_search_step = metadata[d]["max_search_step"]

        table.append(
            {
                "loader": loader_id,
                "trace": trace_id,
                "done": done,
                "timestamp": timestamp,
                "max_search_step": max_search_step,
            }
        )

    def cmp_key(x):
        loader = x["loader"]
        timestamp = x["timestamp"]
        if timestamp is None:
            timestamp = ""
        return f"{loader}_{timestamp}"

    table = sorted(table, key=cmp_key)

    response = jsonify(table)
    return response, 200
