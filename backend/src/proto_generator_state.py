from collections import deque
from typing import Deque, List, Tuple

import numpy as np


class Pose:
    def __init__(self, position: np.ndarray, orientation: np.ndarray = np.zeros(4)):
        self.position = position
        self.orientation = orientation

    def __repr__(self):
        return f"Pose(position={self.position}, orientation={self.orientation})"


class ProtoGeneratorState:
    """
    Record the history state in the proto generator.
    Can be used for temporal info computation, sensor error detection (stale, delayed data), data collection, logging, etc.
    """

    def __init__(self, maxlen=100):
        self.forces_history: Deque[np.ndarray] = deque(maxlen=maxlen)
        self.eef_pose_history: Deque[Pose] = deque(maxlen=maxlen)
        self.timestamp_history: Deque[float] = deque(maxlen=maxlen)
        self.maxlen = maxlen

    def set(
        self,
        forces_history: List[np.ndarray],
        eef_pose_history: List[Pose],
        timestamp_history: List[float],
    ) -> None:
        self.forces_history = deque(forces_history, maxlen=self.maxlen)
        self.eef_pose_history = deque(eef_pose_history, maxlen=self.maxlen)
        self.timestamp_history = deque(timestamp_history, maxlen=self.maxlen)

    def add(self, forces: np.ndarray, eef_pose: Pose, timestamp: float) -> None:
        self.forces_history.append(forces)
        self.eef_pose_history.append(eef_pose)
        self.timestamp_history.append(timestamp)

    def get(self) -> Tuple[Deque[np.ndarray], Deque[Pose], Deque[float]]:
        return self.forces_history, self.eef_pose_history, self.timestamp_history

    def find_latest_state_idx(self, target_timestamp: float) -> int:
        """
        Finds the latest state index before the given target timestamp.
        """
        if len(self.timestamp_history) == 0:
            return 0

        for idx in reversed(range(len(self.eef_pose_history))):
            if self.timestamp_history[idx] <= target_timestamp:
                return idx
        return 0

    def get_recent_velocity(self, duration: float) -> np.ndarray:
        if len(self.eef_pose_history) < 2:
            return np.array([0.0, 0.0, 0])

        current_timestamp = self.timestamp_history[-1]
        lastest_state_idx = self.find_latest_state_idx(current_timestamp - duration)
        displacement = (
            self.eef_pose_history[-1].position
            - self.eef_pose_history[lastest_state_idx].position
        )
        elapsed_time = current_timestamp - self.timestamp_history[lastest_state_idx]
        velocity = displacement / (elapsed_time + 1e-6)
        return velocity
