import matplotlib.pyplot as plt
import numpy as np

# Generate some random data
x_data = np.random.normal(0, 1, 1000)
y_data = np.random.normal(0, 1, 1000)

# Compute the histogram and bin edges
bins = 20
counts, bin_edges = np.histogram(x_data, bins=bins)

# Compute the mean value for each bin
bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2
mean_values = [
    np.mean(y_data[(x_data >= bin_edges[i]) & (x_data < bin_edges[i + 1])])
    for i in range(bins)
]

# Plot the histogram with mean values
fig, ax = plt.subplots()
ax.bar(bin_centers, counts, width=0.05, align="center", color="blue", alpha=0.5)
ax.plot(bin_centers, mean_values, color="red")
ax.set_xlabel("X Values")
ax.set_ylabel("Frequency / Mean Y Values")
ax.set_title("Histogram with Mean Values")
plt.show()
