from typing import List
import matplotlib.pyplot as plt
import numpy as np


class SpringDynamics:
    def __init__(
        self, k_elasticity: float = 1.0, mass: float = 1.0, x_obstacle: float = -0.1
    ):
        self.k_elasticity = k_elasticity
        self.x_obstacle = x_obstacle
        self.mass = mass
        self.x_history = [0, 0]
        self.dt = 0.01
        self.max_acceleration = 1

    def next(self, x: float, noise_level=0):
        """
        F = (x_obstacle - x) * k_elasticity
        """
        self.x_history.append(x)
        self.x_history = self.x_history[-3:]
        acceleration = (self.x_history[-1] - self.x_history[-2]) - (
            self.x_history[-2] - self.x_history[-3]
        ) / self.dt
        acceleration = np.clip(
            acceleration, -self.max_acceleration, self.max_acceleration
        )
        force = (
            self.k_elasticity * max(0, self.x_obstacle - x) + self.mass * acceleration
        )
        force += np.random.uniform(-noise_level, noise_level)
        return force


class Controller:
    def __init__(self, kp: float, ki: float = 0.0, kd: float = 0.0):
        self.kp = kp
        self.ki = ki
        self.kd = kd

        self.previous_error = 0.0
        self.sum_error = 0.0

    def control(self, force: float, force_target: float):
        error = force_target - force
        error_dot = error - self.previous_error
        self.previous_error = error
        self.sum_error += error
        dx = self.kp * error + self.kd * error_dot + self.ki * self.sum_error
        return dx


def clip_acceleration(
    x_list: List[float], x_next: float, max_acceleration: float
) -> float:
    if len(x_list) == 0:
        return x_next

    if len(x_list) == 1:
        x_previous = x_list[-1]
    else:
        x_previous = x_list[-2]

    x_current = x_list[-1]

    velocity = x_current - x_previous
    velocity_next = x_next - x_current

    acceleration = velocity_next - velocity
    acceleration_clipped = np.clip(acceleration, -max_acceleration, max_acceleration)
    velocity_next_clipped = velocity + acceleration_clipped

    x_next_clipped = x_current + velocity_next_clipped
    return x_next_clipped


def test_parameters(k_elasticity=10, mass=15.0, kp=0.001, ki=0.0, kd=0):
    dynamics = SpringDynamics(k_elasticity=k_elasticity, mass=mass)
    controller = Controller(kp=kp, ki=ki, kd=kd)

    x = 0
    force_target = 10

    x_history = []
    force_history = []
    alpha = 0
    for i in range(3000):
        force = dynamics.next(x)
        dx = controller.control(force=force, force_target=force_target)
        x_new = x - dx
        x = clip_acceleration(x_history, x_new, max_acceleration=0.001)
        # x = alpha * x + (1 - alpha) * x_new

        x_history.append(x)
        force_history.append(force)
    return x_history, force_history


def plot():
    # kd = -0.00005
    kd = 0
    x_history, force_history = test_parameters(kd=kd)
    plt.plot(x_history, label=f"kd={kd}")
    plt.plot(force_history, label=f"force")

    # plt.ylim([-10, 10])
    plt.legend()
    plt.show()


def plot_kd():
    for kd in np.linspace(-0.05, 0.05, 5):
        x_history = test_parameters(kd=kd)
        plt.plot(x_history, label=f"kd={kd}")
    plt.ylim([-100, 100])
    plt.legend()
    plt.show()


def plot_mass():
    for mass in np.linspace(0.001, 0.03, 5):
        x_history = test_parameters(mass=mass, kd=-0.05)
        plt.plot(x_history, label=f"mass={mass}")
    plt.ylim([-100, 100])
    plt.legend()
    plt.show()


def main():
    plot()
    # plot_kd()
    # plot_mass()


if __name__ == "__main__":
    main()
