import sys
from typing import List
import numpy as np


class TightMetrics:
    @staticmethod
    def internal_compute_average_jerk(curr_pos_list: List[np.ndarray], elapsed_time_list: List[float]) -> float:
        try:
            common_length = min(len(curr_pos_list), len(elapsed_time_list))
            if common_length < 4 or len(set(elapsed_time_list)) <= 1:
                return 0.0
            time_difference = np.array(np.diff(elapsed_time_list[:common_length])).reshape((-1, 1))
            time_difference[time_difference == 0.0] = np.mean(time_difference)
            velocity = np.diff(curr_pos_list[:common_length], axis=0) / (
                np.abs(time_difference) + sys.float_info.epsilon
            )
            acceleration = np.diff(velocity, axis=0) / (np.abs(time_difference[1:]) + sys.float_info.epsilon)
            jerk = np.diff(acceleration, axis=0) / (np.abs(time_difference[2:]) + sys.float_info.epsilon)
            return np.mean(np.linalg.norm(jerk, axis=-1))
        except Exception as exception:
            return 0.0

    @staticmethod
    def internal_compute_average_control_frequency_hz(elapsed_time_list: List[float]) -> float:
        if len(elapsed_time_list) <= 1:
            return 0.0

        return (len(elapsed_time_list) - 1) / (
            np.abs(elapsed_time_list[-1] - elapsed_time_list[0]) + sys.float_info.epsilon
        )