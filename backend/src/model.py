import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
import joblib

class Policy:
    def __init__(self):
        # self.model = LogisticRegression()
        self.model = DecisionTreeClassifier()

    def fit(self, X_train, Y_train):
        self.model.fit(X_train, Y_train)

    def eval(self, X_test, Y_test):
        Y_pred = self.model.predict(X_test)
        return np.mean(Y_pred == Y_test)

    def save(self, filename='decision_tree.joblib'):
        joblib.dump(self.model, filename)

    def load(self, filename='decision_tree.joblib'):
        self.model = joblib.load(filename)