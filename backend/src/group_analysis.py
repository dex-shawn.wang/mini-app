import json
import os
import random
from typing import List
from matplotlib import pyplot as plt
import numpy as np
import traceback
from tight_metrics import TightMetrics
from model import Policy

from dataloader import get_tight_packing_data, read_group_data
import json


class GroupAnalysis:
    def __init__(self) -> None:
        self.data = {}

    def load_trace(self, trace_name: str):
        loader_id, trace_id = trace_name.split("/")
        data = get_tight_packing_data(loader_id, trace_id)
        return data

    def load_group(self, trace_list: List[str]):
        self.data = {}
        for trace_name in trace_list:
            try:
                d = self.load_trace(trace_name)
                self.data[trace_name] = d
            except Exception as exception:
                print(f"Trace_name {trace_name}; Exception: {exception}")
                # traceback.print_exc()

    def calculate_duration(self):
        duration_list = []
        for trace_name, trace_data in self.data.items():
            duration = 0
            if "elapsed_time_list" in trace_data:
                elapsed_time_list = trace_data["elapsed_time_list"]
                if len(elapsed_time_list) > 0:
                    duration = elapsed_time_list[-1] - elapsed_time_list[0]
            duration_list.append(duration)
        print(
            f"Average duration: {np.mean(duration_list):.1f} +/- {np.std(duration_list):.1f}"
        )
        return duration_list

    def calculate_done(self):
        done_list = []
        for trace_name, trace_data in self.data.items():
            done = False
            if "done" in trace_data:
                done = trace_data["done"]
            done_list.append(done)
        return done_list

    def calculate_interval(self):
        interval_list = []
        for trace_name, trace_data in self.data.items():
            duration = 0
            if "elapsed_time_list" in trace_data:
                elapsed_time_list = trace_data["elapsed_time_list"]
                if len(elapsed_time_list) > 1:
                    for i in range(len(elapsed_time_list) - 1):
                        interval_list.append(
                            elapsed_time_list[i + 1] - elapsed_time_list[i]
                        )
                        if interval_list[-1] > 1:
                            print(
                                "Timeout: ",
                                trace_name,
                                interval_list[-1],
                                elapsed_time_list[i],
                                elapsed_time_list[i + 1],
                            )
        print(
            f"Average interval_list: {np.mean(interval_list):.4f} +/- {np.std(interval_list):.4f}; min: {np.min(interval_list):.4f}; max: {np.max(interval_list):.4f}"
        )
        return interval_list

    def calculate_success_rate(self):
        total = 0
        success_count = 0
        for trace_name, trace_data in self.data.items():
            if "done" in trace_data:
                done = trace_data["done"]
                if done > 0:
                    success_count += 1
            total += 1
        success_rate = success_count / total if total > 0 else 0
        return success_rate, success_count, total

    def calculate_start_pos(self, curr_pos_list):
        """
        Calculate the start position of the trace after approaching phase.
        Return the first pos.z that descend > start_dz_tol.
        """
        start_dz_tol = 0.001
        for curr_pos in curr_pos_list:
            if curr_pos[2] < curr_pos_list[0, 2] - start_dz_tol:
                return curr_pos
        return curr_pos_list[0]

    def calculate_wiggle(self):
        wiggle_range_list = []
        offset_list = []
        for trace_name, trace_data in self.data.items():
            if "curr_pos" in trace_data:
                curr_pos = np.array(trace_data["curr_pos"])
                start_pos = self.calculate_start_pos(curr_pos)
                if len(curr_pos) > 0:
                    offset_list.append(curr_pos[-1, 1] - start_pos[1])
                    wiggle_range = np.max(curr_pos[:, 1]) - np.min(curr_pos[:, 1])
                    wiggle_range_list.append(wiggle_range)
        print(
            f"Avg offset: {np.mean(np.abs(offset_list)):.3f} +/- {np.std(np.abs(offset_list)):.3f} m"
        )
        print(
            f"Wiggle range: {np.mean(wiggle_range_list):.3f} +/- {np.std(wiggle_range_list):.3f} m"
        )
        return offset_list, wiggle_range_list

    def calculate_average_jerk(self):
        average_jerk_list = []
        for trace_name, trace_data in self.data.items():
            if "curr_pos" in trace_data and "elapsed_time_list" in trace_data:
                average_jerk_list.append(
                    TightMetrics.internal_compute_average_jerk(
                        trace_data["curr_pos"], trace_data["elapsed_time_list"]
                    )
                )
                print(f"average_jerk: {average_jerk_list[-1]}")
        return average_jerk_list

    def calculate_average_frequency(self):
        average_frequency_list = []
        for trace_name, trace_data in self.data.items():
            if "elapsed_time_list" in trace_data:
                average_frequency_list.append(
                    TightMetrics.internal_compute_average_control_frequency_hz(
                        trace_data["elapsed_time_list"]
                    )
                )
                print(f"average_frequency: {average_frequency_list[-1]}")
        return average_frequency_list

    def _compuse_approached_pos(self, curr_pos: np.ndarray) -> np.ndarray:
        dz_threshold = 0.001
        for i in range(len(curr_pos)):
            if curr_pos[i, 2] < curr_pos[0, 2] - dz_threshold:
                print(f"idx after approach: {i}")
                return curr_pos[i]

        return curr_pos[-1]

    def _compute_single_policy_data(
        self,
        curr_pos: np.ndarray,
        force_history: np.ndarray,
        search_routine_state_step_count: np.ndarray,
        diff_z_tol: float = 0.1,
    ):
        """
        Compute the policy data for a single trace.
        """
        feature_list = []
        label_list = []
        start_pos = self.calculate_start_pos(curr_pos)
        approached_pos = self._compuse_approached_pos(curr_pos)
        future_step = 0
        STEP = 100
        for i in range(STEP, len(curr_pos)):
            if curr_pos[i, 2] >= start_pos[2]:
                continue

            while (
                future_step < len(curr_pos)
                and curr_pos[future_step, 2] >= curr_pos[i, 2] - diff_z_tol
            ):
                future_step += 1

            if future_step >= len(curr_pos):
                break

            if (
                search_routine_state_step_count[i] < 10
                or search_routine_state_step_count[i] > 50
            ):
                continue

            label = curr_pos[future_step, 1] - curr_pos[i, 1]

            if np.abs(label) <= 0.005:
                continue

            label_list.append(label)

            features = []
            for j in range(i - STEP + 1, i + 1, 1):
                if j == i - STEP + 1:
                    dY = 0
                    dZ = 0
                    dTx = 0
                    dFz = 0
                else:
                    dY = curr_pos[j, 1] - curr_pos[j - 1, 1]
                    dZ = curr_pos[j, 2] - curr_pos[j - 1, 2]
                    dTx = force_history[j, 1] - force_history[j - 1, 1]
                    dFz = force_history[j, 2] - force_history[j - 1, 2]

                feature = [
                    force_history[i][1],
                    force_history[i][2],
                    # dTx,
                    # dFz,
                    dY,
                    dZ,
                ]
                features.extend(feature)
            # dY0 = curr_pos[i, 1] - curr_pos[0, 1]
            # dY0 = curr_pos[i, 1] - approached_pos[1]
            dY0 = 0
            features.extend([dY0])
            feature_list.append(features)
        return feature_list, label_list

    def compute_policy_data(self):
        feature_list = []
        label_list = []
        for trace_name, trace_data in self.data.items():
            if "curr_pos" in trace_data:
                curr_pos = np.array(trace_data["curr_pos"])
                force_hisotry = np.array(trace_data["force"])
                search_routine_state_step_count = np.array(
                    trace_data["search_routine_state_step_count"]
                )
                if len(curr_pos) > 0:
                    (
                        feature_list_single,
                        label_list_single,
                    ) = self._compute_single_policy_data(
                        curr_pos, force_hisotry, search_routine_state_step_count
                    )
                    feature_list.extend(feature_list_single)
                    label_list.extend(label_list_single)
        feature_list = np.array(feature_list)
        label_list = np.array(label_list) >= 0
        return feature_list, label_list


def visualize_histogram(x_data, y_data, x_interval=0.01):
    x_min = np.min(x_data)
    x_max = np.max(x_data)
    bin_count = int(np.ceil(x_max / x_interval))
    x_range = np.arange(0, bin_count) * x_interval

    y_list = []
    for x in x_range:
        mask = (x_data >= x) & (x_data < x + x_interval)
        if np.sum(mask) == 0:
            y_mean = 0
        else:
            y_mean = np.mean(y_data[mask])
        y_list.append(y_mean)
    print("x_range", x_range, "y_list", y_list)
    # plt.plot(x_range, y_list, "o")
    plt.bar(x_range + x_interval / 2, y_list, width=x_interval * 0.9)


def visualize_offset_time(offset, duration, figsize=(15, 5), output_fn=None):
    x_data = np.abs(offset)
    y_data = np.array(duration)
    visualize_histogram(x_data, y_data)
    # plt.figure(figsize=figsize)
    # plt.plot(np.abs(offset), duration, "o")
    # # plt.hist2d(np.abs(offset), duration, bins=50, cmap="plasma")

    plt.xlabel("Offset (y_end - y_start) m")
    plt.ylabel("Duration (s)")

    if output_fn:
        plt.savefig(output_fn)
    # plt.show()


def visualize_interval(interval_list, figsize=(15, 5), output_fn=None):
    interval_list = np.array(interval_list)
    # interval_list = interval_list[interval_list > 0.02]
    print(len(interval_list[interval_list < 0.1]) / len(interval_list))
    print(len(interval_list[interval_list > 0.1]))
    plt.figure()
    plt.hist(interval_list, bins=100)
    # plt.figure(figsize=figsize)
    # plt.plot(np.abs(offset), duration, "o")
    # # plt.hist2d(np.abs(offset), duration, bins=50, cmap="plasma")
    plt.yscale("log")
    plt.xlabel("Interval (s)")
    plt.ylabel("Count")

    if output_fn:
        plt.savefig(output_fn)
    # plt.show()


def visualize_policy_data(feature_list, label_list, output_fn=None):
    feature_list = np.array(feature_list)
    label_list = np.array(label_list)
    print(feature_list.shape, label_list.shape)
    plt.figure()
    plt.plot(feature_list, label_list, ".", markersize=1)
    plt.xlabel("Force (N)")
    plt.ylabel("Offset (m)")
    plt.axhline(y=0, color="black")
    plt.axvline(x=0, color="black")
    plt.xlim([-200, 200])
    plt.ylim([-0.2, 0.2])
    if output_fn:
        plt.savefig(output_fn)
    # plt.show()


def dump_data(
    X_test: np.ndarray,
    Y_test: np.ndarray,
    sample_size=100,
    fn_data="checkpoints/sample_test_data.json",
):
    dirname = os.path.dirname(__file__)
    fn_data = os.path.join(dirname, fn_data)

    sample_idx = np.random.choice(len(X_test), sample_size, replace=False)
    data = {
        "X_test": X_test[sample_idx].tolist(),
        "Y_test": Y_test[sample_idx].tolist(),
    }
    with open(fn_data, "w") as f:
        json.dump(data, f)


def test_model():
    seed = 0
    np.random.seed(seed)

    group_analysis = GroupAnalysis()
    fn_group = "data/groups/group_all.json"
    trace_list = read_group_data(fn_group)

    random.seed(0)
    random.shuffle(trace_list)
    trace_list_train = trace_list[: int(len(trace_list) * 0.8)]
    trace_list_test = trace_list[int(len(trace_list) * 0.8) :]
    group_analysis.load_group(trace_list_train)
    X_train, Y_train = group_analysis.compute_policy_data()

    group_analysis.load_group(trace_list_test)
    X_test, Y_test = group_analysis.compute_policy_data()

    print("X_train.shape", X_train.shape, Y_train.shape)
    print("X_test.shape", X_test.shape, Y_test.shape)

    dump_data(
        X_test, Y_test, sample_size=100, fn_data="checkpoints/sample_test_data.json"
    )

    policy = Policy()
    policy.fit(X_train, Y_train)

    dirname = os.path.dirname(__file__)
    fn_model = os.path.join(dirname, "checkpoints", "decision_tree.joblib")
    policy.save(fn_model)
    policy.load(fn_model)

    result = policy.eval(X_test, Y_test)
    print(f"accuracy {result*100:.1f} %")


def sort_duration_done(duration, done):
    zipped = list(zip(duration, done))
    zipped.sort(key=lambda x: x[0])
    duration_sorted, done_sorted = zip(*zipped)
    return duration_sorted, done_sorted


def visualize_duration_done(duration, done, figsize=(15, 5), output_fn=None):
    duration, done = sort_duration_done(duration, done)

    plt.figure(figsize=figsize)
    x = np.arange(len(duration))
    color = ["C0" if d else "C1" for d in done]
    plt.bar(x, duration, color=color)
    plt.xlabel("id")
    plt.ylabel("duration (s)")
    plt.ylim(0, 43)

    if output_fn:
        plt.savefig(output_fn)
    # plt.show()


def main():
    group_analysis = GroupAnalysis()
    # fn_group = "data/groups/group_1.json"
    # fn_group = "data/groups/group_all.json"
    # fn_group = "data/groups/group_speed_up.json"
    # fn_group = "data/groups/group_dexcore2_20231030.json"
    fn_group = "data/groups/group_dexcore2_20231115-16.json"
    # fn_group = "data/groups/group_dexcore1_20231031.json"
    # fn_group = "data/groups/group_dexcore1_20231114.json"
    trace_list = read_group_data(fn_group)
    group_analysis.load_group(trace_list)
    duration_list = group_analysis.calculate_duration()
    done_list = group_analysis.calculate_done()
    success_rate, success_count, total = group_analysis.calculate_success_rate()
    print(f"success_rate {(success_rate * 100):.1f}% ({success_count} / {total})")

    offset_list, wiggle_range_list = group_analysis.calculate_wiggle()

    output_fn = f"figures/offset_duration.png"
    visualize_offset_time(offset_list, duration_list, output_fn=output_fn)

    # interval_list = group_analysis.calculate_interval()
    # output_fn = f"figures/interval.png"
    # visualize_interval(interval_list, output_fn=output_fn)

    feature_list, label_list = group_analysis.compute_policy_data()
    visualize_policy_data(feature_list, label_list)

    output_fn = f"figures/duration_done.png"
    TIEMOUT = 30
    duration_success_list = np.array(duration_list)[np.array(done_list) == 1]
    print(
        f"ratio < {TIEMOUT}: {len(duration_success_list[duration_success_list < TIEMOUT]) / len(duration_success_list)}"
    )
    visualize_duration_done(duration_list, done_list, output_fn=output_fn)

    average_jerk = group_analysis.calculate_average_jerk()
    print(f"average_jerk: {np.mean(average_jerk):.1f} +/- {np.std(average_jerk):.1f}")

    average_frequency = group_analysis.calculate_average_frequency()
    print(
        f"average_frequency: {np.mean(average_frequency):.1f} +/- {np.std(average_frequency):.1f}"
    )


if __name__ == "__main__":
    main()
    # test_model()
