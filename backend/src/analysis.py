import os
import time
import cv2
import numpy as np
import matplotlib.pyplot as plt
import json
import datetime
from scipy.spatial.transform import Rotation as R
import trimesh
from dataloader import get_data

from pallet_analysis import process_obstacles, visualize_pallet


def visualize_force(forces, figsize=(15, 5), output_fn=None):
    if len(forces) == 0:
        forces = np.array([[0, 0, 0]])
    forces = np.array(forces)
    torque_x = forces[:, 1]
    torque_y = forces[:, 0]
    force_z = forces[:, 2]
    plt.figure(figsize=figsize)
    plt.plot(torque_x, label="torque_x")
    plt.plot(force_z, label="force_z")
    plt.plot(torque_y, label="torque_y")
    plt.xlabel("time (steps ~0.01s)")
    plt.ylabel("force (N) / torque (0.1 Nm)")
    plt.legend()

    if output_fn:
        plt.savefig(output_fn)
    # plt.show()


def visualize_search_routine_state(
    search_routine_steps, figsize=(15, 5), output_fn=None
):
    if len(search_routine_steps) == 0:
        search_routine_steps = [0]
    plt.figure(figsize=figsize)
    plt.plot(search_routine_steps, label="search_routine_state_step_count")
    plt.xlabel("time (steps ~0.01s)")
    plt.ylabel("search_routine_state_step_count")
    plt.legend()

    if output_fn:
        plt.savefig(output_fn)
    # plt.show()


def visualize_eef(curr_pos, goal_pos=np.zeros(3), figsize=(15, 5), output_fn=None):
    curr_pos = np.array(curr_pos)
    curr_pos -= goal_pos
    if len(curr_pos) == 0:
        curr_pos = np.array([[0, 0, 0]])

    plt.figure(figsize=figsize)
    plt.plot(curr_pos[:, 1], label="y")
    plt.plot(curr_pos[:, 2], label="z")
    plt.xlabel("time (steps ~0.01s)")
    plt.ylabel("position (m)")
    plt.legend()

    if output_fn:
        plt.savefig(output_fn)
    # plt.show()


def visualize_2d_path(curr_pos, figsize=(15, 5), output_fn=None):
    curr_pos = np.array(curr_pos)
    if len(curr_pos) == 0:
        curr_pos = np.array([[0, 0, 0]])
    plt.figure(figsize=figsize)
    colors = np.linspace(0, 1, len(curr_pos))
    plt.scatter(
        curr_pos[:, 1],
        curr_pos[:, 2],
        marker=".",
        c=colors,
        s=25,
        cmap=plt.get_cmap("cool"),
    )
    plt.colorbar(label="Start to end")
    plt.xlabel("y (m)")
    plt.ylabel("z (m)")

    if output_fn:
        plt.savefig(output_fn)
    # plt.show()


def get_done(tight_packing_data):
    done = False
    if "done" in tight_packing_data:
        done = tight_packing_data["done"]
    return done


def process(loader_id, trace_id, always_update=True):
    data = get_data(loader_id, trace_id)

    tight_packing_data = data["msg"]["tightpack"]

    intentional_offset_y = tight_packing_data["intentional_offset_y"]
    forces = tight_packing_data["force"]
    search_routine_state_step_count = tight_packing_data[
        "search_routine_state_step_count"
    ]
    done = get_done(tight_packing_data)

    curr_pos = tight_packing_data["curr_pos"]

    goal_pos = np.zeros(3)
    final_pos = (np.array(curr_pos[-1]) * 1000).astype(int) / 1000
    print(f"final pos {final_pos}")

    output_dir = f"figures/{loader_id}"
    os.makedirs(output_dir, exist_ok=True)

    output_fn = f"figures/{loader_id}/{trace_id}.png"
    if not os.path.exists(output_fn) or always_update:
        visualize_force(forces, output_fn=output_fn)

    output_fn = f"figures/{loader_id}/{trace_id}_search_routine.png"
    if not os.path.exists(output_fn) or always_update:
        visualize_search_routine_state(
            search_routine_state_step_count, output_fn=output_fn
        )

    output_fn = f"figures/{loader_id}/{trace_id}_eef.png"
    if not os.path.exists(output_fn) or always_update:
        visualize_eef(curr_pos, output_fn=output_fn, goal_pos=goal_pos)

    output_fn = f"figures/{loader_id}/{trace_id}_pallet.png"
    if not os.path.exists(output_fn) or always_update:
        pallet_state = process_obstacles(data)
        visualize_pallet(pallet_state, output_fn=output_fn)

    output_fn = f"figures/{loader_id}/{trace_id}_eef_2D_path.png"
    if not os.path.exists(output_fn) or always_update:
        visualize_2d_path(curr_pos, output_fn=output_fn)


if __name__ == "__main__":
    # loader_id = "drex1"
    # trace_id = "f74a9bc96a397ca1"
    # process(loader_id, trace_id)

    loader_id = "drex6"
    # trace_id = "661a646e862b3107"
    trace_id = "d92ddce38749b8f9"
    process(loader_id, trace_id)
