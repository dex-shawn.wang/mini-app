import datetime
import json

import numpy as np


def get_datetime(timestamp: float):
    return datetime.datetime.fromtimestamp(timestamp).strftime("%Y-%m-%d %H:%M:%S.%f")[
        :-3
    ]


def get_max_search_step(tight_packing_data) -> int:
    if "search_routine_state_step_count" not in tight_packing_data:
        return 0
    search_routine_state_step_count = tight_packing_data[
        "search_routine_state_step_count"
    ]
    if len(search_routine_state_step_count) == 0:
        return 0
    return np.max(search_routine_state_step_count)


def get_meta_data(data):
    timestamp_formated = None
    if "start_timestamp" in data:
        timestamp_formated = get_datetime(data["start_timestamp"])
    done = None
    if "done" in data:
        done = data["done"]
    max_search_step = get_max_search_step(data)

    return {
        "done": done,
        "timestamp": timestamp_formated,
        "max_search_step": str(max_search_step),
    }


def get_meta_data_from_filename(filename):
    with open(filename, "r") as f:
        data = json.load(f)
    return get_meta_data(data["msg"]["tightpack"])
