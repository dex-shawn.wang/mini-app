import json
import os
from typing import Dict

from data_parser import get_meta_data


def get_data(loader: str, trace_id: str):
    filename = f"data/{loader}/LOG_PACK_{trace_id}.json"
    with open(filename, "r") as f:
        data = json.load(f)
    return data


def get_tight_packing_data(loader_id, trace_id):
    data = get_data(loader_id, trace_id)
    tight_packing_data = data["msg"]["tightpack"]
    return tight_packing_data


def read_group_data(fn_group):
    with open(fn_group, "r") as file:
        data = json.load(file)
    return data


class DataLoader:
    def __init__(self):
        self._index_filename = "data/index.json"
        self._meta_filename = "data/metadata.json"
        self.data = ["drex1/LOG_PACK_f74a9bc96a397ca1.json"]
        self.metadata: Dict[str, Dict[str, str]] = {}
        os.makedirs("data", exist_ok=True)
        self.load()

    def load(self) -> None:
        if not os.path.exists(self._index_filename):
            self.save()
        if not os.path.exists(self._meta_filename):
            self.save_meta()

        with open(self._index_filename, "r") as f:
            json_data = f.read()
        data = json.loads(json_data)
        self.data = data

        with open(self._meta_filename, "r") as f:
            json_data = f.read()
        data = json.loads(json_data)
        self.metadata = data

    def save(self) -> None:
        json_data = json.dumps(self.data)
        with open(self._index_filename, "w") as f:
            f.write(json_data)

    def save_meta(self) -> None:
        json_data = json.dumps(self.metadata)
        with open(self._meta_filename, "w") as f:
            f.write(json_data)

    def update(self, loader_id, trace_id_list, recent_duration=3) -> None:
        print("trace_id_list: ", trace_id_list)
        update_list = []

        for trace_id in trace_id_list:
            filename = f"{loader_id}/LOG_PACK_{trace_id}.json"
            if filename not in self.data or not os.path.exists(f"data/{filename}"):
                update_list.append(trace_id)
        print("update_list: ", update_list)
        if len(update_list) == 0:
            return

        os.system(
            f"./scripts/get_planner_log.sh {','.join(update_list)} {loader_id} {recent_duration}"
        )

        downloaded_update_list = []
        for trace_id in update_list:
            filename = f"{loader_id}/LOG_PACK_{trace_id}.json"
            if filename not in self.data and os.path.exists(f"data/{filename}"):
                downloaded_update_list.append(trace_id)

        for trace_id in downloaded_update_list:
            filename = f"{loader_id}/LOG_PACK_{trace_id}.json"
            if filename not in self.data:
                self.data.append(filename)
        self.save()

    def update_meta(self, loader_id, trace_id_list) -> None:
        for trace_id in trace_id_list:
            filename = f"{loader_id}/LOG_PACK_{trace_id}.json"
            if filename not in self.metadata:
                if not os.path.exists(f"data/{filename}"):
                    return

                data = get_tight_packing_data(loader_id, trace_id)
                self.metadata[filename] = get_meta_data(data)

        self.save_meta()


def load_trace_id_list(loader_id: str) -> None:
    filename_loader = f"data/tmp/LOG_PACK_tight_packing.json"
    try:
        with open(filename_loader, "r") as f:
            json_data = f.read()
        data = json.loads(json_data)
    except Exception as exception:
        print("Exception: loading TIGHT PACKING trace id list", exception)
        data = []
    return data


def main() -> None:
    data_loader = DataLoader()
    data_loader.load()
    data_loader.update_meta("drex2", load_trace_id_list("drex2"))
    print(data_loader.data)


if __name__ == "__main__":
    main()
