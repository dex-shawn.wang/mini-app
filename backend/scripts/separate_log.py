import json
import os
import sys
from gcp_migration import migrate_single_trace

FLAG_WRITE_TO_DATA_GCP = True

trace_list = sys.argv[1]
loader_id = sys.argv[2]


def main():
    if trace_list == "tight_packing":
        return
    filename = "data/tmp/LOG_PACK_{}.json".format(trace_list)
    current_path = os.path.dirname(os.path.realpath(__file__))
    output_gcp_path = os.path.join(current_path, "..", "raw_logs")

    data = {}
    with open(filename) as f:
        for json_obj in f:
            json_dict = json.loads(json_obj)
            print("seperate logs json dict.keys(): ", json_dict.keys())
            for trace_id in json_dict:
                data[trace_id] = json_dict[trace_id]

    print(data.keys())
    for trace_id in data:
        print("Processing trace {}".format(trace_id))
        if trace_id in data:
            print("Saving trace {}".format(trace_id))
            filename_id = "data/{}/LOG_PACK_{}.json".format(loader_id, trace_id)
            with open(filename_id, "w") as f:
                f.write(json.dumps(data[trace_id]))

            if FLAG_WRITE_TO_DATA_GCP:
                migrate_single_trace(filename_id, output_gcp_path)

    os.remove(filename)


if __name__ == "__main__":
    main()
