#!/bin/bash

# Usage:
#   all traces: ./scripts/get_planner_log.sh tight_packing drex7 1
#   single trace: ./scripts/get_planner_log.sh <trace> drex7 1

TRACE=${1}
HOSTNAME=${2:drex1}
RECENT_DURATION=${3:0}
VIA_SSH=${4:true}
TMP_REMOTE_DIR=/tmp
LOG_DIRECTORY=/var/log/dex/truck/optimus

echo "IN get_planner_log.sh ${RECENT_DURATION}"

echo "Retrieve traces of ${TRACE}."
echo "Retrieve planner json logs from ${HOSTNAME}."

echo "VIA_SSH: ${VIA_SSH}"
if [[ $VIA_SSH == "false" ]]; then
  echo "Local run."
  cp ./scripts/get_planner_log_from_trace.py ${TMP_REMOTE_DIR}
else
  echo "Remote run."
  scp ./scripts/get_planner_log_from_trace.py ${HOSTNAME}:${TMP_REMOTE_DIR}
fi

CMD_CREATE='mkdir -p /tmp/decompressed_planner_log;'
CMD_UNZSTD='for file in /var/log/dex/truck/coordinator/coordinator_drex-tcy/*.zst; do yes n | unzstd -o /tmp/decompressed_planner_log/$(basename "${file}" .zst) "$file"; done; '
CMD_UNZSTD_EXTRA='for file in /var/log/dex/truck/coordinator/coordinator_drex8/*.zst; do yes n | unzstd -o /tmp/decompressed_planner_log/$(basename "${file}" .zst) "$file"; done; '
CMD_UNZSTD_EXTRA_ALL='for file in /var/log/dex/truck/coordinator/coordinator_'${HOSTNAME}'/*.zst; do yes n | unzstd -o /tmp/decompressed_planner_log/$(basename "${file}" .zst) "$file"; done; '
CMD_COORD='cp /var/log/dex/truck/coordinator/coordinator_drex-tcy/current /tmp/decompressed_planner_log/current_left;'
CMD_COORD_CURRENT_DREX1='cp /var/log/dex/truck/coordinator/coordinator_drex1/current /tmp/decompressed_planner_log/current_left;'
CMD_COORD_CURRENT_ALL="cp /var/log/dex/truck/coordinator/coordinator_${HOSTNAME}/current /tmp/decompressed_planner_log/current_left;"
CMD_RUN='cd '"${TMP_REMOTE_DIR}"' ; python get_planner_log_from_trace.py '"${TRACE} ${RECENT_DURATION}"

CMD="${CMD_CREATE} ${CMD_UNZSTD} ${CMD_UNZSTD_EXTRA} ${CMD_UNZSTD_EXTRA_ALL} ${CMD_UNZSTD_EXTRA_DREX1} ${CMD_COORD} ${CMD_COORD_CURRENT_ALL} ${CMD_RUN}"



if [[ $VIA_SSH == "false" ]]; then
  bash -c "${CMD}"
else
  ssh -t ${HOSTNAME} ${CMD}
fi

if [[ $HOSTNAME == "drex0" ]]; then
  mkdir -p /tmp/decompressed_planner_log;
  cp -rf ~/log/dex/truck/optimus/agent_left_planner_DREX1/* /tmp/decompressed_planner_log/;
  cp -rf ~/log/dex/truck/optimus/agent_right_planner_DREX1/* /tmp/decompressed_planner_log/;
  rm /tmp/decompressed_planner_log/current;
  cp -rf ~/log/dex/truck/optimus/agent_left_planner_DREX1/current /tmp/decompressed_planner_log/current_left;
  cp -rf ~/log/dex/truck/optimus/agent_right_planner_DREX1/current /tmp/decompressed_planner_log/current_right;
fi

mkdir -p data/${HOSTNAME}
# scp dexterity@${HOSTNAME}:${TMP_REMOTE_DIR}/LOG_PACK_${TRACE:0:32}.json data/${HOSTNAME}/
mkdir -p data/tmp

if [[ $VIA_SSH == "false" ]]; then
  echo "Local run finished."
else
  scp ${HOSTNAME}:${TMP_REMOTE_DIR}/LOG_PACK_${TRACE:0:32}.json data/tmp/LOG_PACK_${TRACE:0:32}.json
  python scripts/separate_log.py ${TRACE:0:32} ${HOSTNAME}
fi