"""
A simple script to be run in the robot server to get planner logs. The used libraries should be core from python since
we do not have a python environment in the server
"""

import datetime
import json
import os
import re
import sys
import time


# LOG_DIRECTORY = "/var/log/dex/truck/optimus/"  # path to directory containing folders for each agent of with the logs
LOG_DIRECTORY = "/tmp/decompressed_planner_log/"  # path to directory containing folders for each agent of with the logs
LOG_TIGHT_PACKING_DIRECTORY = "/data/log/tight_packing/"  # path to upload to GCP
trace = sys.argv[1]  # trace of the log to retrive from file or in a folder

recent_duration = 3  # in days; 0 means all logs
if len(sys.argv) > 2:
    recent_duration = float(sys.argv[2])

# # to extract logs locally, uncomment the following
# LOG_DIRECTORY = os.path.join(os.getenv('HOME'),'log/dex/truck/optimus') # path to directory containing folders for each agent of with the logs
# trace = "160df678fda5ecce" # trace of the log to retrive from file or in a folder


def get_planner_log_for_trace(trace_id_list, filename):
    """
    Retrives logs specific to a trace from the filename.
    """
    planner_log_list = {}
    tightpack_log_list = {}
    planner_key = "planner"
    tightpack_key = "tightpack"
    if not os.path.exists(filename):
        return {}, None
    with open(filename) as f:
        for json_obj in f:
            try:
                json_dict = json.loads(json_obj)["msg"]
                trace_id = json_dict["trace_id"]
                if trace_id in trace_id_list:
                    print("FOUND!!!!!!!!!!!!!!!! trace_id", trace_id)
                    if planner_key in json_dict:
                        planner_log_list[trace_id] = json_dict
                    if tightpack_key in json_dict:
                        print("FOUND!!!!!!!!!!!!!!!! trace_id TIGHPACK", trace_id)
                        tightpack_log_list[trace_id] = json_dict
            except Exception as e:
                # print("exception", e)
                continue
    return planner_log_list, tightpack_log_list


def save_log_file(filename, msg_log, overwrite=False):
    if overwrite:
        with open(filename, "w") as fp:
            json.dump(msg_log, fp)
    else:
        with open(filename, "a+") as fp:
            # Move read cursor to the start of file.
            fp.seek(0)
            # If file is not empty then append '\n'
            line = fp.readline()
            if "msg" in line:
                fp.write("\n")
            json.dump(msg_log, fp)


def get_trace_id_list(trace_id, filename):
    """
    Retrives trace_id that includes tight packing
    """
    tightpack_trace_id_list = []
    planner_key = "planner"
    tightpack_key = "tightpack"
    if not os.path.exists(filename):
        return []
    with open(filename) as f:
        for json_obj in f:
            try:
                json_dict = json.loads(json_obj)["msg"]
                if tightpack_key in json_dict:
                    tightpack_trace_id_list.append(json_dict["trace_id"])
            except Exception as e:
                continue
    return tightpack_trace_id_list


def main_tight_packing():
    tightpack_trace_id_list = []
    if os.path.exists(LOG_DIRECTORY):
        files = []
        for root, a, filenames in os.walk(LOG_DIRECTORY):
            for filename in filenames:
                if is_recent_timestamp(filename, recent_duration):
                    files.append(os.path.join(root, filename))
        for f in files:
            print("Running in server filename", f)
            trace_list = get_trace_id_list(trace_id=trace, filename=f)
            tightpack_trace_id_list += trace_list
            filename_tight_traces = "LOG_PACK_tight_packing.json"
            save_log_file(
                filename_tight_traces, tightpack_trace_id_list, overwrite=True
            )
    return tightpack_trace_id_list


def get_timestamp_in_second(filename):
    pattern = r".*@(\d+)\.log.*"
    if "current" in filename:
        return 1e20
    results = re.findall(pattern, filename)
    if len(results) == 0:
        return 0
    timestamp = int(results[0])
    return timestamp / 1e9


def is_recent_timestamp(directory_path, recent_duration):
    timestamp = get_timestamp_in_second(directory_path)
    try:
        print(
            directory_path,
            datetime.datetime.fromtimestamp(timestamp).strftime("%Y-%m-%d %H:%M:%S.%f")[
                :-3
            ],
        )
    except Exception as e:
        print("exception", e)
    if recent_duration == 0:
        return True
    current_time = time.time()
    elapsed_days = (current_time - timestamp) / (24.0 * 3600)
    print("elapsed_days", elapsed_days, "recent_duration", recent_duration)
    if elapsed_days < recent_duration:
        return True
    return False


def extract_log_from_trace_id_list(trace, via_ssh=True):
    # filename_pack = "LOG_PACK_" + trace + ".json"
    trace_id_list = trace.split(",")
    filename_pack = "LOG_PACK_" + trace[:32] + ".json"
    if os.path.exists(filename_pack):
        os.remove(filename_pack)

    print("STARTING searching logs for trace", trace_id_list)
    results = {}
    if os.path.exists(LOG_DIRECTORY) and trace:
        files = []
        for root, a, filenames in os.walk(LOG_DIRECTORY):
            for filename in filenames:
                if is_recent_timestamp(filename, recent_duration):
                    files.append(os.path.join(root, filename))
        filename = "LOG_" + trace + ".log"
        if os.path.exists(filename):
            os.remove(filename)
        for f in files:
            (planner_logs, tightpacking_logs) = get_planner_log_for_trace(
                trace_id_list=trace, filename=f
            )
            print("Running in server filename", f)

            for trace_id in planner_logs:
                # Save tightpacking log
                if trace_id not in tightpacking_logs:
                    continue
                log = tightpacking_logs[trace_id]
                log["obstacles"] = planner_logs[trace_id]["planner"]["snapshot"][
                    "world"
                ]["obstacles"]
                log["metadata"] = planner_logs[trace_id]["planner"]["metadata"]
                msg_log = {"msg": log}

                if via_ssh:
                    save_log_file(filename_pack, {trace_id: msg_log})
                else:
                    filename = os.path.join(
                        LOG_TIGHT_PACKING_DIRECTORY, "LOG_PACK_{}.json".format(trace_id)
                    )
                    print("saving filename", filename)
                    save_log_file(filename, msg_log)


def main_log_collector():
    trace_id_list = main_tight_packing()
    print("trace_id_list", trace_id_list)
    trace_joined = ",".join(trace_id_list)
    extract_log_from_trace_id_list(trace_joined, via_ssh=False)


def main():
    if trace == "tight_packing":
        main_tight_packing()
        return
    elif trace == "log_collector":
        main_log_collector()
        return

    extract_log_from_trace_id_list(trace, via_ssh=True)


def test_check_recent_log():
    filename = "'@1682442256345444784.log'"
    timestamp = get_timestamp_in_second(filename)
    print("check_recent_log", is_recent_timestamp(filename, 3))


if __name__ == "__main__":
    main()
    # test_check_recent_log()
