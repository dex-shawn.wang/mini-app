import datetime
import glob
import json
import os
import shutil
import subprocess
import sys


def get_loader_id(filename="data/drex1/LOG_PACK_72ef112f2368c9bd.json"):
    return filename.split("/")[-2]


def get_loader_hostname(loader_id="drex1"):
    loader_number = int(loader_id.lstrip("drex"))
    return "trcombodrex{}.tra.fdx.idm.dex.ai".format(loader_number)


def get_trace_id(filename="data/drex1/LOG_PACK_72ef112f2368c9bd.json"):
    return filename.split("/")[-1].split("_")[-1].rstrip(".json")


def get_date_from_data(data):
    timestamp = float(data["msg"]["tightpack"]["start_timestamp"])
    MM_DD_YYYY = datetime.datetime.fromtimestamp(timestamp).strftime("%m_%d_%Y")
    return MM_DD_YYYY


def get_date(filename="data/drex1/LOG_PACK_72ef112f2368c9bd.json"):
    with open(filename, "r") as f:
        data = json.load(f)

    return get_date_from_data(data)


def get_output_filename(filename="data/drex1/LOG_PACK_72ef112f2368c9bd.json", output_path="raw_logs"):
    loader_id = get_loader_id(filename=filename)
    loader_hostname = get_loader_hostname(loader_id)
    trace_id = get_trace_id(filename=filename)
    date = get_date(filename=filename)
    output_filename = os.path.join(
        output_path, date, loader_hostname, "LOG_PACK_{}.json".format(trace_id)
    )
    return output_filename

def migrate_single_trace(
    filename="data/drex1/LOG_PACK_72ef112f2368c9bd.json", output_path="raw_logs"
):
    try:
        output_filename = get_output_filename(filename, output_path)
    except Exception as e:
        print("Skipping file: {} {}".format(filename, e))
        return

    if not os.path.exists(output_filename):
        os.makedirs(os.path.dirname(output_filename), exist_ok=True)
        shutil.copyfile(filename, output_filename)


def main():
    current_path = os.path.dirname(os.path.realpath(__file__))
    data_path = os.path.join(current_path, "..", "data")
    output_path = os.path.join(current_path, "..", "raw_logs")
    filenames = glob.glob(data_path + "/drex*/LOG_PACK_*.json")

    for filename in filenames:
        migrate_single_trace(filename, output_path)


if __name__ == "__main__":
    main()
