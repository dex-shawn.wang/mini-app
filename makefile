docker.build:
	make -C backend docker.build
	make -C ui docker.build
docker.pull:
	make -C backend docker.pull
	make -C ui docker.pull
docker.push:
	make -C backend docker.push
	make -C ui docker.push