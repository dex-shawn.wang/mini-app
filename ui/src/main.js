import { createApp } from 'vue'
import { createStore } from 'vuex'
import App from './App.vue'

import './assets/main.css'

// Create a new store instance.
const store = createStore({
    state () {
      return {
        count: 0,
        trace: {
          "loader": "drex1",
          "trace": "f74a9bc96a397ca1"
        }
      }
    },
    mutations: {
      increment (state) {
        state.count++
      }
    }
  })

const app = createApp(App)
app.use(store)
app.mount('#app')
